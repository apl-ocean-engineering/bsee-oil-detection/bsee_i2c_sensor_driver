# Combined driver for BME280 and INA260 i2c sensors

ROS driver for the BME280 pressure-temperature-humidity sensor and INA260 power-current sensor included in the BSEE control stack.   These two sensors are on the same I2C bus, so we're using to unified driver to control I2C bus contention (Linux may actually take care of this transparently, but this is simpler).

Uses Adafruit's Circuitpython libraries for the two sensors ([BME280](https://github.com/adafruit/Adafruit_CircuitPython_BME280) and [INA260](https://github.com/adafruit/Adafruit_CircuitPython_INA260)), and the [Blinka](https://circuitpython.org/blinka/odroid_c4/) package to run the Circuitpython modules.

# Prerequisites

This package depends on the Adafruit Blinka and the Circuitpython modules for the two sensors, installed with pip:

```
pip3 install Adafruit-blinka adafruit-circuitpython-bme280 adafruit-circuitpython-ina260
```

# ROS node

The ROS node `bsee_i2c_sensor_driver` takes the following parameters:

* `~bme280_i2c_address` (string):  The I2C address of the BME280 sensor, can be a decimal or a hex digit `0xXX`
* `~ina260_i2c_address` (string): The I2C address of the INA260 sensor, can be a decimal or a hex digit `0xXX`
* `~frame_id` (string): The value assigned to the "header.frame" field in published messages.
* `~bme_rate` (float):  The rate (in Hz) for querying the BME280.  If <= 0, the sensor is not queried.
* `~ina_rate` (float):  The rate (in Hz) for querying the INA260.  If <= 0, the sensor is not queried.

It published the following topics:

* `htp`  ([bosch_sensortec_msgs/Bme280Htp](https://gitlab.com/apl-ocean-engineering/mcneil-auv-lab/bosch_sensortec_msgs/-/blob/main/msg/Bme280Htp.msg?ref_type=heads))   Combined humidity, temperature and pressure from the BME280 sensor.
* `humidity` ([sensor_msgs/RelativeHumidity](https://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/RelativeHumidity.html))  Humidity measurement.
* `temperature` ([sensor_msgs/Temperature](https://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Temperature.html)) Temperature.
* `pressure` ([sensor_msgs/FluidPressure](https://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/FluidPressure.html))  Ambient pressure.
* `power` ([power_monitoring_msg/VoltageCurrentPower](https://gitlab.com/apl-ocean-engineering/bsee-oil-detection/power_monitor_msgs/-/blob/main/msg/VoltageCurrentPower.msg?ref_type=heads))  Combined voltage, current and power measurement from the INA260 sensor.


# Sample scripts

The `samples/` directory contains simple (non-ROS) Python test scripts.

# LICENSE

This code is released under the [BSD 3-Clause License](LICENSE).  Some codes in `samples/` is copies from the [Adafruit Learning System](https://learn.adafruit.com/).
