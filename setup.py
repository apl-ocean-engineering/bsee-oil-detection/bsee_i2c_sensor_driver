#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
    packages=["bsee_i2c_sensor_driver"], package_dir={"": "src"}
)

setup(**setup_args)
