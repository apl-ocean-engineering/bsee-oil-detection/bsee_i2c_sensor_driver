#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
import board
import busio
import adafruit_ina260
from adafruit_bme280 import basic as adafruit_bme280

i2c = busio.I2C(board.SCL1, board.SDA1)  # uses i2c-1 SCL and SDA

# i2c = board.STEMMA_I2C()  # For using the built-in STEMMA QT connector on a microcontroller

ina260 = adafruit_ina260.INA260(i2c, address=0x45)
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c, address=0x77)

while True:
    print(
        "Current: %.2f mA Voltage: %.2f V Power:%.2f mW"
        % (ina260.current, ina260.voltage, ina260.power)
    )

    print("Temperature: %0.1f C" % bme280.temperature)
    print("Humidity: %0.1f %%" % bme280.humidity)
    print("Pressure: %0.1f hPa" % bme280.pressure)

    time.sleep(1)
