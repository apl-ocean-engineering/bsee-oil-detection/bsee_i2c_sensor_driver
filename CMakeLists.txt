cmake_minimum_required(VERSION 3.0.2)
project(bsee_i2c_sensor_driver)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  rospy
  bosch_sensortec_msgs
  power_monitor_msgs
)

catkin_python_setup()

catkin_package(
  CATKIN_DEPENDS
      rospy
      bosch_sensortec_msgs
      power_monitor_msgs
)

catkin_install_python(PROGRAMS
  nodes/bsee_i2c_sensor_driver
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY launch/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)


## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
