#! /usr/bin/env python3

import rospy

from bosch_sensortec_msgs.msg import Bme280Htp
from sensor_msgs.msg import Temperature, RelativeHumidity, FluidPressure
from power_monitor_msgs.msg import VoltageCurrentPower

import board
import busio
import adafruit_ina260
from adafruit_bme280 import basic as adafruit_bme280


class Bme280Ina260Driver:
    def __init__(self) -> None:

        self.bme_addr = rospy.get_param("~bme280_i2c_address")
        self.ina_addr = rospy.get_param("~ina260_i2c_address")

        self.frame_id = rospy.get_param("~frame_id")

        bme_hz = rospy.get_param("~bme_rate")
        if bme_hz > 0:
            self.bme_period = 1.0 / bme_hz
        else:
            self.bme_period = 0.0

        ina_hz = rospy.get_param("~ina_rate")
        if ina_hz > 0:
            self.ina_period = 1.0 / ina_hz
        else:
            self.ina_period = 0.0

        self.htp_pub = rospy.Publisher("bme280/htp", Bme280Htp, queue_size=1)
        self.temp_pub = rospy.Publisher("bme280/temperature", Temperature, queue_size=1)
        self.humid_pub = rospy.Publisher(
            "bme280/humidity", RelativeHumidity, queue_size=1
        )
        self.pressure_pub = rospy.Publisher(
            "bme280/pressure", FluidPressure, queue_size=1
        )

        self.ina_pub = rospy.Publisher(
            "ina260/power", VoltageCurrentPower, queue_size=1
        )

        self.i2c = busio.I2C(board.SCL1, board.SDA1)  # uses i2c-1 SCL and SDA

        # int(..., base=0) means "try to guess the base"
        self.ina260 = adafruit_ina260.INA260(
            self.i2c, address=int(self.ina_addr, base=0)
        )
        self.bme280 = adafruit_bme280.Adafruit_BME280_I2C(
            self.i2c, address=int(self.bme_addr, base=0)
        )

    def run(self) -> None:
        if self.bme_period > 0:
            self.bme_timer = rospy.Timer(
                rospy.Duration(self.bme_period), self.bme_callback
            )

        if self.ina_period > 0:
            self.ina_timer = rospy.Timer(
                rospy.Duration(self.ina_period), self.ina_callback
            )

        rospy.spin()

    def bme_callback(self, event) -> None:

        htp_msg = Bme280Htp()
        htp_msg.header.stamp = rospy.Time.now()
        htp_msg.header.frame_id = self.frame_id

        htp_msg.humidity = self.bme280.humidity
        htp_msg.temperature = self.bme280.temperature
        # Library reports pressure in hectopascals (1 hPa = 100 Pa)
        htp_msg.pressure = self.bme280.pressure * 100
        self.htp_pub.publish(htp_msg)

        temp_msg = Temperature()
        temp_msg.header = htp_msg.header
        temp_msg.temperature = htp_msg.temperature
        temp_msg.variance = 0
        self.temp_pub.publish(temp_msg)

        humid_msg = RelativeHumidity()
        humid_msg.header = htp_msg.header
        # Library reports in percent, while sensor_msgs expects [0.0, 1.0].
        humid_msg.relative_humidity = htp_msg.humidity / 100.0
        humid_msg.variance = 0
        self.humid_pub.publish(humid_msg)

        pressure_msg = FluidPressure()
        pressure_msg.header = htp_msg.header
        pressure_msg.fluid_pressure = htp_msg.pressure
        pressure_msg.variance = 0
        self.pressure_pub.publish(pressure_msg)

    def ina_callback(self, event) -> None:

        msg = VoltageCurrentPower()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = self.frame_id

        msg.voltage = self.ina260.voltage
        msg.current = self.ina260.current / 1000.0  # Convert mA to A
        msg.power = self.ina260.power / 1000.0  # convert mW to W

        self.ina_pub.publish(msg)


def main() -> None:
    rospy.init_node("bme280_ina260_driver")
    driver = Bme280Ina260Driver()
    driver.run()
